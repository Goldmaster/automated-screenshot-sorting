# Automated Screenshot Sorting

This is a Bash script that automates the sorting of screenshots into app specific folders based on the name of the app that generated the screenshot.

This idea was originally inspired by [RoboYoshi`s](https://github.com/roboyoshi) [Datacurator File tree screenshots folder](https://github.com/roboyoshi/datacurator-filetree/tree/main/root/images/screenshots).

## Getting started 

1. Clone or download this repository to your computer.

2. Open the `sort-screenshots.sh` script and update the `input_directory` variable with the path to the directory where your screenshots are stored, and `output_directory` for where you want the screenshots to go.
If you are using WSL, then ensure file paths have forwarded slashes and the start of your folder starts as `/mnt/` due to the script mainly designed for Linux usage. Remember to hit save.

3. Open a terminal and navigate to the directory where the script is stored.

4. Run the script by typing `./sort-screenshots.sh` and pressing Enter.

5. The script will automatically create a folder for each app and move the corresponding screenshots to the appropriate folder.

## Usage on Unraid

1. Browse to the userscripts plugin

2. click add new script and give it a name such as `Automated Screenshot Sorting`.

3. Click on the cog icon next to the name you have given the script, and choose `edit script`.

4. paste the script contents (ensure you only have one `#!/bin/bash` line at the top).

5. Here, update the `input_directory` variable with the path to the directory where your screenshots are stored, and `output_directory` for where you want the screenshots to go (both parts are in yellow like text).

6. Next hit save chances to save the script.

7. (Optional) You can set a schedule of when to run the script, either weekly or monthly. **Make sure to test your script beforehand.**


## Dependencies

This script requires Bash to run. It has been tested on macOS and Ubuntu, but should work on any Unix-based system. Windows users need to use Windows sub system for Linux for this to work.

For Unraid, ensure the [Userscripts plugin](https://unraid.net/community/apps?q=Squid%27s+Repository&r=0#r) is installed.

## Licence

This script is released under the MIT Licence. See the `LICENSE` file for more information.


## Credits

This script and readme was coded vastly thanks to [ChatGPT](https://chat.openai.com/chat/e5f0bd87-0c66-46f8-b544-0afb8adce75d), as I am not a programmer. But did have to spend some time training ChatGPT to generate the right code though trial and error.
