#!/bin/bash

# Set the directory where the files are located
input_directory="$(realpath -m '/mnt/c/PATH/TO/SCREENSHOTS')"
# Set the directory where the app-specific folders will be created
output_directory="$(realpath -m '/mnt/c/PATH/TO/images/screenshots/')"

# Loop through each PNG file in the directory
for file in "$input_directory"/*.png
do
    # Get the file name without the directory path and the ".png" extension.
    filename=$(basename "$file" .png)
    
    # Extract the name of the app from the filename and replace spaces with hyphens.
    appname=$(echo "$filename" | cut -d'_' -f3- | tr '[:upper:]' '[:lower:]' | tr ' ' '-')
    
    # Create the app-specific directory if it does not exist
    if [ ! -d "$output_directory/$appname" ]
    then
        mkdir "$output_directory/$appname"
    fi
    
    # Move the file to the app-specific directory
    mv "$file" "$output_directory/$appname/"
done
